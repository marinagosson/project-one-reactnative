import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';

import ButtonComp from './components/ButtonComp';

export default class ProjectOne extends Component {

  constructor(props){
    super(props);
    this.state = {
      user : '',
      password : ''
    };
  }

  onChangeUser(user){
    this.state.user = user;
  };

  onChangePassword(password){
    this.state.password = password;
  };

  onClickLogin(){
    if (this.state.user === '') {
      alert('user vazio');
    } else {
      if (this.state.password === '') {
        alert('password vazio');
      } else {
        alert('Joia ' + this.state.user);
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>

      <Text>Usuário</Text>

      <TextInput style={styles.textInput}
      onChangeText={(user) => {this.onChangeUser(user)}}/>

      <Text>Senha</Text>

      <TextInput style={styles.textInput}
      onChangeText={(password) => {this.onChangePassword(password)}}/>

      <ButtonComp text='Login' onPress={() => {this.onClickLogin()}} />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  textInput: {
    width : 200,
    padding : 10,
    justifyContent: 'center',
    borderWidth : 0.5,
    borderRadius : 10,
    borderColor : 'blue',
    backgroundColor : 'white'
  }
});

AppRegistry.registerComponent('ProjectOne', () => ProjectOne);
