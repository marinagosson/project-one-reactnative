import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

export default class ButtonComp extends React.Component {
  render() {
    return(
      <TouchableOpacity onPress={this.props.onPress}>
        <View style = {styles.buttonComp}>
              <Text style= {[styles.title, styles.activeTitle]}>
                {this.props.text}
              </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  buttonComp: {
    backgroundColor : 'blue',
    paddingVertical : 10,
    paddingHorizontal : 20,
    borderRadius : 10,
    borderWidth : 0.5,
    borderColor : 'red'
  },
  title: {
    fontSize : 19,
    fontWeight : 'bold'
  },
  activeTitle: {
    color: 'white'
  }
});

module.exports = ButtonComp;
